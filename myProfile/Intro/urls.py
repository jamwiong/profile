from django.urls import path

from . import views

app_name = 'Intro'

urlpatterns = [
    path('', views.index, name='index'),
]